module.exports = function(grunt) {
  grunt.initConfig({
    watch: {
      options: {
        livereload: true,
      },
      src: {
        files: ['webdev/sass/*', 'webdev/js/*', 'webdev/js/*/**', 'webdev/index.php'],
        tasks: ['sass','autoprefixer']
      }
    },
    sass: {
      options: {
        style: 'compact',
        banner: '/*  S P C L O P S  */'
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'webdev/sass',
          src: ['main.scss'],
          dest: 'webdev/css',
          ext: '.css'
        }]
      }
    },
    autoprefixer: {
      dist: {
        files: {
          'webdev/css/main.css': 'webdev/css/main.css'
        }
      }
    },
    shell: {
      bake:{
        options: {
          stdout: true
        },
        command: 'Console/Cake bake'
      },
      build:{
        options: {
          stdout: true
        },
        command: [
					'r.js -o webdev/build/build.js',
          'cd webroot',
          'rm -rf build build.txt sass test.php js/vendor/backbone-amd  js/vendor/jquery  js/vendor/underscore-amd  js/vendor/wow  js/vendor/zepto'
        ].join('&&')
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-shell');


  // Bake!
  grunt.registerTask('bake', 'shell:bake');
  // Build with require.js' r.js
  grunt.registerTask('build', 'shell:build');
  // Default task <grunt>
  grunt.registerTask('default', 'watch');
};
