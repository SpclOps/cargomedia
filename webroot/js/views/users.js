define(['backbone'], function(Backbone){
	App.Views.Users = Backbone.View.extend({
		tagName: 'ul',
		render: function(){
			// this.collection.each(function(user){
			// 	var userView = new App.Views.User({ model: user });
			// 	this.$el.append(userView.render().el);
			// });
			// console.log(userView.render().el);
			return this;
		}
	});

	App.Views.User = Backbone.View.extend({
		tagName: 'li',
		template: template('userTemplate'),
		fullName: function(){
			return this.get('firstName') + ' ' + this.get('surname');
		}
		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}

	});

	return App.Views.User;
	return App.Views.Users;
});
