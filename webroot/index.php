<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Social Graph Action!</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,600,900' rel='stylesheet' type='text/css'>
	<link href="img/favicon.ico" type="image/x-icon" rel="icon" />
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<!--[if lt IE 9]>
	  <p class="update">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<header class="main-head">
		<h1>Escorting Precious Cargo...</h1>
	</header>


	<main id="main-content" class="main-content">

		<div class="container">
	    <div class="row md6">

				<div class="row text-center">
					<select class="md4">
		        <option>{ guid }</option>
		        <option>{ guid }</option>
		        <option>{ guid }</option>
		        <option>{ guid }</option>
		        <option>{ guid }</option>
			    </select>
			    <div class="node md4">
				    <img src="/img/total-recall.jpg" style="width:80px; border-radius:2px;" class="left">
				    <h4>Rob Fitz</h4>
				    <p>Age: 23 | Sex: Male</p>
	        </div>
				</div>

			    <div class="row">
		        <ul class="friends">
		          <li style="background:#fff;">Direct Friends
		            <ul>
	                <li class="person">
	                	<span>1</span>
										<ul class="guid-values">
											<li>guid values</li>
										</ul>
	                </li>
	                <li class="person">
	                	<span>3</span>
	                	<ul class="guid-values">
											<li>guid values</li>
										</ul>
									</li>
		            </ul>
		          </li>
		          <li style="background:#ffc;">Friends of Friends
		            <ul>
	                <li class="person">
	                	<span>4</span>
										<ul class="guid-values">
											<li>guid values</li>
										</ul>
	                </li>
	                <li class="person">
	                	<span>7</span>
	                	<ul class="guid-values">
											<li>guid values</li>
										</ul>
									</li>
		            </ul>
		          </li>
		          <li style="background:#fdd;">Suggested Friends
		            <ul>
	                <li class="person">
	                	<span>8</span>
										<ul class="guid-values">
											<li>guid values</li>
										</ul>
	                </li>
	                <li class="person">
	                	<span>2</span>
	                	<ul class="guid-values">
											<li>guid values</li>
										</ul>
									</li>
		            </ul>
		          </li>
		        </ul>        
			    </div>    

      </div>
    </div>

	</main>

	<footer class="main-foot">

		<a href="http://www.madeinbasel.org/"><img src="img/basel.png" width="160px" class="basel right"/></a>
	</footer>

	<script type="text/template" id="guidsTemplate">
		<h3>Guids Template!</h3>
	</script>
	<script data-main="js/main.js" src="js/vendor/requirejs/require.js"></script>
</body>
</html>
