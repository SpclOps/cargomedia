<?php
$api['guids'] = array();

$url = "../data.json";
$json = file_get_contents($url);

$jsonIterator = new RecursiveIteratorIterator(
						    new RecursiveArrayIterator(json_decode($json, TRUE)),
										RecursiveIteratorIterator::SELF_FIRST);

foreach ($jsonIterator as $key => $val)
{
	if (is_array($val)) 
	{
		// Each Node of Social Graph Representation (data.json loop)
		if (array_key_exists('firstName', $val))
		{
				$user['name'] = "".$val['firstName']." ".$val['surname']."";
				$api['guids'][] = md5($user['name']);
		}
	}
}

print json_encode($api['guids']);