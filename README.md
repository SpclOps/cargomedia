#Cargo Media Social Graph Task
---

##Task - Social Graph
This task is designed to give us an idea of how you approach software development problems. We will look at the structure of any database tables you might create, as well as the structure of your code and whether or not the solution you create is extensible. We are looking at how efficient your algorithms are as well as which existing tools you chose to use and how you integrate them.

##Problem Description
The purpose of this task is to create a method of examining a social network. You are given data (data.json) representing a group of people, in the form of a social graph. Each person listed has one or more connections within the group.

###Come up with a data structure to store and query the information found in the JSON file.
You should then create a public API in the language of your choice which allows for three basic operations to be executed for a certain person:  
Direct friends: Return those people who are directly connected to the chosen person.  
Friends of friends: Return those who are two steps away, but not directly connected to the chosen person.  
Suggested friends: Return people in the group who know 2 or more direct friends of the chosen person, but are not directly connected to her.  
Your API can be exposed as public functions, a REST-endpoint, a command line interface, whatever fits the choice of your technology stack best.

##Requirements
Use object oriented design  
Write unit and/or integration tests to verify your solution works  
Provide information on how to setup, test and use your solution  
Spend max. 3h. Complete within a week (two weeks in special circumstances).

##Suggestions
Use a VCS  
Use appropriate package managers when include 3rd party libraries


---  

##Dev Environment Components & Setup
- Grunt
- Bower
Dependencies: npm (node.js)

###Install Node & Npm
http://nodejs.org
https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager
or something like this:  
echo 'export PATH=$HOME/local/bin:$PATH' >> ~/.bashrc
. ~/.bashrc
mkdir ~/local
mkdir ~/node-latest-install
cd ~/node-latest-install
curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
./configure --prefix=~/local
make install # ok, fine, this step probably takes more than 30 seconds...
curl https://npmjs.org/install.sh | sh


###Inside social_graph directory:
If you don't have Grunt & Bower already on your machine, remove the -g if you don't want it installed globally:  
`npm install -g grunt-cli`  
`npm install -g bower`
`npm install` to install Grunt dependencies  
`bower install` to install Bower dependencies  
You now have tools to use with Grunt in node_modules/ dir and the tasks are listed in gruntfile.js  
##All development is done in the webdev/ dir and then compiled via require.js' r.js using `grunt build` which will compile everything to the webroot/ dir which will be used for production
Use `grunt` in terminal to activate the default grunt task which is set to the 'watch' task which will watch for changes to the sass & js files. If you have the LiveReload browser extension: http://feedback.livereload.com/knowledgebase/articles/86242-how-do-i-install-and-use-the-browser-extensions  

You will see your document updated automatically.  
Along with the sass compiling, the default `grunt` task uses autoprefixer which adds up-to-date vendor prefixes for all of your compiled css using http://caniuse.com as a reference.  

