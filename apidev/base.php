<?php
require_once './vendor/autoload.php';

$app = new Silex\Application();
$app->get('/persons', function () use ($app) {
  return file_get_contents("http://cargomedia.spclops.com/rest.php");
});

$app->get('/person/{guid}', function ($guid) use ($app) {
  return 'Let Me Load the Person with the Hash: '.$app->escape($guid);
});

$app->run();
