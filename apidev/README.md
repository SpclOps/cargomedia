#How to Install

 `$ curl -sS https://getcomposer.org/installer | php`
 `$ composer install`
#How to Run
 `$ php -S 0.0.0.0:59005`

#Unit Testing
 `$ composer install --dev`
 `$ vendor/bin/phpunit`
