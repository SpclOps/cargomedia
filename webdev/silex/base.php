<?php
require_once './vendor/autoload.php';

$app = new Silex\Application();
$app->get('/guids', function () use ($app) {
	  return file_get_contents("http://cargomedia.spclops.com/rest.php");
});

$app->get('/person/{hash}', function ($hash) use ($app) {
		$m = new MongoClient();
		$db = $m->socialgraph;

		$collection = $db->cargomedia;
		$cursor = $collection->findOne(array('hash' => $app->escape($hash)));

		$first = $cursor['name']['first'];
		$last  = $cursor['name']['last'];
		$sex   = $cursor['sex'];
		$age   = $cursor['age'];

	  $person_data = array('name' => array('first' => $first, 'last' => $last),
		                     'info' => array('sex' => $sex, 'age' => $age)
		                    );

		  return json_encode($person_data);
});

$app->put('/guid/{hash}', function ($hash) use ($app) {
	    return json_encode(array('guid' => $hash, 'Person' => file_get_contents('http://home.spclops.com:59005/base.php/person/'.$hash.'')));
});

$app->get('/persons', function () use ($app) {
	  return file_get_contents("http://cargomedia.spclops.com/rest.php");
});


$app->run();
