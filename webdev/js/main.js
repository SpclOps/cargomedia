require.config({
	baseUrl: 'js/',
	paths: {
		'jquery': 'vendor/jquery/dist/jquery.min',
		'underscore': 'vendor/underscore-amd/underscore-min',
		'backbone': 'vendor/backbone-amd/backbone-min'
	},
	shim: {
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'jquery': {
      exports: '$'
    },
    'underscore': {
			exports: '_'
    }
  }
});


(function(){
	window.App = {
		Models: {},
		Collections: {},
		Views: {},
		Routers: {},
	};
})();

require(['jquery','underscore','backbone','router','landing','collections/guids','collections/persons'],
function($,_,backbone,router,landing,guids,persons){

	// By default jQuery doesn't allow us to convert our forms into Javascript Objects, 
	// simply call it via $(form).serializeObject() and get an object returned.
	$.fn.serializeObject = function(){
		var o = {};
		var a = this.serializeArray();
		$.each(a, function(){
			if (o[this.name] !== undefined){
				if (!o[this.name].push){
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

  Backbone.emulateHTTP = true;
	Backbone.emulateJSON = true;

	App.Models.Person = Backbone.Model.extend({
		//urlRoot: 'person-get.php'
    url:'person-get.php'
		//url: function(hash){
    //  var base = this.urlRoot || (this.collection && this.collection.url) || "/";
    //  if (this.isNew()) return base;										  
	  //  return base + "?id=" + encodeURIComponent(this.hash);
	  //}
	});

	App.Views.GuidsList = Backbone.View.extend({
		el: '#selecta',
		events: {
		 'change select': 'hashPass'
		},
		hashPass: function(){
			var hash = this.$('#hashSelect').val();
			//hashPass(hash);
			var personItem = new App.Views.PersonItem();
			personItem.render(hash);
		},
		render: function(){
			var that = this;
			var guids = new App.Collections.Guids();
			guids.fetch({
				success: function(guids){
					var template = _.template($('#guidsTemplate').html(), { guids: guids.models });
					console.log(guids);
					that.$el.html(template);
				}
			});
		}
	});

	App.Views.PersonsList = Backbone.View.extend({
		el: '#personCurrent',
		template: _.template($('#personsListTemplate').html(), { persons: persons.models }),
		hashPass: function(){
			var hash = this.$('#hashSelect').val();
			var persons = new App.Collections.Persons();
			var that = this;
			console.log('prefetch:');
		  console.log(persons);
			persons.fetch({
				data: $.param({hash:hash}),
				processData: true,
				success: function(){
					console.log('fetch:');
					console.log(persons);
					that.$el.html(that.template);
				}
			});
			that.$el.html(that.template);
			console.log('post fetch:');
			console.log(persons);
		},
		render: function(persons){
			this.$el.html(this.template);
		}
	});

	App.Views.PersonItem = Backbone.View.extend({
		el: '#personItem',
		template: _.template($('#personItemTemplate').html()),
		render: function(hash){
			var that = this;
			var person = new App.Models.Person();
			person.fetch({
				data: $.param({hash:hash}),
				processData: true,
				success: function(person){
					console.log('hashPass GET:');
					console.log(person);
					that.$el.html(that.template({ person: person }));	
			  	return this;
				}
			});
		}
	});

	App.Views.PersonAdd = Backbone.View.extend({
		el: '#directFriends',
		events: {
			'submit .person-form': 'savePerson'
		},
		template: _.template($('#personAddTemplate').html()),
		savePerson: function(e){
			var personDetails = $(e.currentTarget).serializeObject();
			var person = new App.Models.Person();
			person.save(personDetails, {
				success: function(person){
					router.navigate('', {trigger: true});
				}
			});
			return false
		},
		//render: function(){
		//	this.collection.each(function(person){
		//		var personView = new App.Views.Person({model: person});
		//		this.$el.append(personView.render().el);
		//	}, this);
		//	return this;
		//}
		render: function(){
			this.$el.html(this.template());
		}
	});

	var guidsList = new App.Views.GuidsList;
	var personsList = new App.Views.PersonsList;
	var personAdd = new App.Views.PersonAdd;

	var router = new App.Routers.Router();
	var hashPass = function(hash){
		var that = this;
		var persons = new App.Collections.Persons();
		var person = new App.Models.Person();
		person.fetch({
			data: $.param({hash:hash}),
			processData: true,
			success: function(person){
				console.log('hashPass GET:');
				console.log(person);
				var personItem = new App.Views.PersonItem;
				var template = _.template($('#personItemTemplate').html());
				$('#personItem').append(template({ person: person }));	
			}
		});
		//Backbone.ajax({
		//	dataType: "jsonp",
		//	url: "http://cargomedia.spclops.com/person-get.php?hash="+hash+"",
		//	data: "",
		//	success: function(val){
		//		console.log(val);
		//		console.log('backbone.ajax GET:');
		//		console.log(persons);
		//   // personsList.render();
		//	}
		//});
	}
	router.on('route:index', function(){
		guidsList.render();
	});
	router.on('route:add', function(){
		personAdd.render();
	});
	router.on('route:hashPass', function(hash){	

	});
	

	Backbone.history.start({ pushState: true });
});



