define(['backbone'], function(Backbone){
	App.Routers.Router = Backbone.Router.extend({
		routes: {
			'': 'index',
			'add': 'add',
			'silex/base.php/person/*hash':'hashPass'
			//'person/*hash':'hashPass'
		}
	});
	
	return App.Routers.Router;
});
