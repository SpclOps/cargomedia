define(['backbone'], function(Backbone){
	App.Collections.Users = Backbone.Collection.extend({
		model: App.Models.User,
		url: '../../../data.json'
	});

  return App.Collections.Users;
});