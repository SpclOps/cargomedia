define(['backbone'], function(Backbone){
	App.Collections.Guids = Backbone.Collection.extend({
		url: 'guid-get.php'
	});
	return App.Collections.Guids;
});
