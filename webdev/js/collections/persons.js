define(['backbone'], function(Backbone){
	App.Collections.Persons = Backbone.Collection.extend({
		url: 'person-get.php'
	});
	return App.Collections.Persons;
});
		
