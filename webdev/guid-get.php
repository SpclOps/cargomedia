<?php
// Connect
$m = new MongoClient();

// Select Database
$db = $m->socialgraph;

// Selection of Collection
$collection = $db->cargomedia;

// Find Everything in the Social Graph
$cursor = $collection->find();

// Loop Through the Collection (Social Graph) on the Server
$guids = array();
$i = 0;
foreach ($cursor as $key => $document) {
	$guids[$i] = array('guids' => $document['hash']);
	$i++;
}
header('Content-Type: application/json');
echo json_encode($guids);
