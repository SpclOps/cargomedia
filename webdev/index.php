<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Social Graph Action!</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,600,900' rel='stylesheet' type='text/css'>
	<link href="img/favicon.ico" type="image/x-icon" rel="icon" />
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<!--[if lt IE 9]>
	  <p class="update">You are using an <strong>ouliated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<header class="main-head">
		<h1>Escorting Precious Cargo...</h1>
	</header>


	<main id="main-content" class="main-content">
    <div class="row sm8 md6">

			<section class="row">
				<div id="selecta" class="xs12 md6 md-push3 text-center"></div>
				<div id="personItem" class="single md4"></div>
			</section>

	  <!---  <section class="row">
        <ul class="friends">
          <li style="background:#fff;">Direct Friends
            <ul id="directFriends">
              
            </ul>
          </li>
          <li style="background:#ffc;">Friends of Friends
            <ul>
              <li class="person">
              	<span>4</span>
								<ul class="guid-values">
									<li>guid values</li>
								</ul>
              </li>
              <li class="person">
              	<span>7</span>
              	<ul class="guid-values">
									<li>guid values</li>
								</ul>
							</li>
            </ul>
          </li>
          <li style="background:#fdd;">Suggested Friends
            <ul>
              <li class="person">
              	<span>8</span>
								<ul class="guid-values">
									<li>guid values</li>
								</ul>
              </li>
              <li class="person">
              	<span>2</span>
              	<ul class="guid-values">
									<li>guid values</li>
								</ul>
							</li>
            </ul>
          </li>
        </ul>        
	    </section>    
-->
    </div>
	</main>

	<footer class="main-foot">
		<a href="http://www.madeinbasel.org/"><img src="img/basel.png" width="160px" class="basel right"/></a>
	</footer>


	<script type="text/template" id="directTemplate">
		<li>This is a direct friend</li>
		
	</script>
	<script type="text/template" id="personAddTemplate">
		<form class="person-form">
			<input type="text" name="name" placeholder="Name"/>	
			<input type="text" name="age" placeholder="Age"/>
			<button type="submit">Add Person</button>
		</form>
	</script>
	<script type="text/template" id="personItemTemplate">
		<div class="user-get xs12 md4 left">
			<h3><strong>GET</strong> user/{guid}</h3>
		</div>
		<div class="person-item xs12 md8 left">
			<h3><%= person.get('name') %></h3>
			<h3><%= person.get('sex') %></h3>
			<h3><%= person.get('age') %></h3>
		</div>
	</script>
	<script type="text/template" id="personsListTemplate">
		<h3>Persons Template:</h3>
		<ul id="personWrap" class="sm6 md6">
		<% _.each(persons, function(person){ %>
			<li><%= person.get('age') %></li>
		<% }); %>
		</ul>
	</script>
	<script type="text/template" id="guidsTemplate">
	<div class="selecta">
		<h3>Select your Global Unique ID :</h3>
		<div class="select-wrap">
			<select id="hashSelect" class="sm8 md6">
			<option>Select..</option>
			<% _.each(guids, function(guid){ %>
				<option><%= guid.get('guids') %></option>
			<% }); %>
			</select>
		</div>
	</div>
	</script>
	<script data-main="js/main.js" src="js/vendor/requirejs/require.js"></script>
</body>
</html>
