<?php
if (!isset($argv[1])) {
  $hash = trim($_GET['hash']);
} else {
	$hash = trim($argv[1]);
}

if (!isset($hash)) { echo "ERROR HASH IS REQUIRED AS GET VARIABLE OR CLI PARAM"; };

// Connect
$m = new MongoClient();

// Select Database
$db = $m->socialgraph;

// Selection of Collection
$collection = $db->cargomedia;

// Find Everything in the Social Graph
$cursor = $collection->findOne(array('hash' => $hash));

// Send to Browser
header('Content-Type: application/json');
echo json_encode(array( 'name' => "".$cursor['name']['first']." ".$cursor['name']['last']."",
											 'age' => $cursor['age'],
											 'sex' => $cursor['sex']));
